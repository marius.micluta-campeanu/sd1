# Sisteme distribuite - Tema 1

## Cerințe generale:

  * Sistem de tipul Client - Server (mai mulți clienți și un server).
  * Metoda de procesare a datelor pe Server trebuie să fie făcută concurent - folosind go routines. 

  * Există un fișier de configurare în care există parametrii inițiali ai programului. 
    * Ex: câte elemente are array-ul de date pe care îl poate trimite clientul, de câte ori se apelează o routină go. Decideți voi ce anume puteți include în acest fișier.

  * Trebuie să existe mesaje între client și server de tipul: 
    * Client `<Nume>` Conectat.
    * Client `<Nume>` a facut request cu datele: `<date>`.
    * Server a primit requestul.
    * Server proceseaza datele.
    * Server trimite `<raspuns>` catre client.
    * Client `<Nume>` a primit raspunsul: `<raspuns>`.


## Cerințe individuale:

  * Clientul trimite către server un array de numere naturale. 
  * Serverul returnează către client suma elementelor array-ului format din dublarea primei cifre a fiecărui număr.
    * Exemplu: 23, 43, 26, 74 => suma de returnat = 223 + 443 + 226 + 774.

## Exemplu de rulare

`go run server.go -c config.txt`

`go run client.go -n 'Mihai Eminescu'`

`go run client.go`

![](sd_tema_1.png)