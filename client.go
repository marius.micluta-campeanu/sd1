package main

import (
    "bufio"
    config "gitlab.com/marius.micluta-campeanu/sd1/config"
    "encoding/binary"
    "flag"
    "fmt"
    "net"
    "os"
    "regexp"
    "strconv"
    "strings"
)

func main() {
    name := flag.String("n", "default", "Set client name")
    file := flag.String("c", "config.txt", "Path to config file")
    flag.Parse()

    protocol, port, n, err := config.ReadOptionsFromFile(*file)
    if err != nil {
        fmt.Println(err)
        return
    }

    conn, err := net.Dial(protocol, port)

    if err != nil {
        fmt.Println(err)
        return
    }
    fmt.Printf("Client %s conectat\n", *name)

    reader := bufio.NewReader(os.Stdin)
    fmt.Print("Introduceți numerele: ")
    text, _ := reader.ReadString('\n')
    removeBadCharacters := func(r rune) rune {
        if r >= '0' && r <= '9' || r == ',' {
            return r;
        }
        return -1;
    }
    text = strings.Map(removeBadCharacters, text)
    if len(text) < 1 {
        fmt.Println("Bad input. Abort!")
        return    // skip empty input
    }
    // trim extra commas
    re := regexp.MustCompile(`\A,+|,+\z`)
    text = re.ReplaceAllString(text, "")
    re = regexp.MustCompile(`,+`)
    text = re.ReplaceAllString(text, ",")
    raw_numbers := strings.Split(text, ",")
    // if input is only commas, skip
    test_empty_input := strings.Replace(strings.Join(raw_numbers, ""), ",", "", -1)
    if len(test_empty_input) < 1 {
        fmt.Println("Bad input. Abort!")
        return
    }
    var i uint32
    input_length := uint32(len(raw_numbers))
    n2 := func(a, b uint32) uint32 {
        if a < b {
            return a;
        }
        return b
    }(n, input_length)

    fmt.Printf("Client %s a făcut request cu datele:", *name)
    bytes := make([]byte, 65)
    for i = 0; i < n2; i++ {
        fmt.Printf(" %s", raw_numbers[i])
        number, _ := strconv.ParseUint(raw_numbers[i], 10, 64)
        binary.LittleEndian.PutUint64(bytes, number)
        bytes[64] = '\n'
        conn.Write(bytes)
    }
    if n > input_length {
        for i = 0; i < n - input_length; i++ {
            fmt.Print(" 0")
            number := uint64(0)
            binary.LittleEndian.PutUint64(bytes, number)
            bytes[64] = '\n'
            conn.Write(bytes)
        }
    }
    fmt.Println()
    result_bytes, _ := bufio.NewReader(conn).ReadBytes('\n')
    result := uint64(binary.BigEndian.Uint64(result_bytes))
    fmt.Printf("Client a primit răspunsul: %d\n", result)
}
