/*
    Micluta-Campeanu Marius
    Grupa 452
    > go run server.go
    > telnet 127.0.0.1 8080
*/

package main

import (
    "bufio"
    config "gitlab.com/marius.micluta-campeanu/sd1/config"
    "encoding/binary"
    "flag"
    "fmt"
    "log"
    "net"
    "os"
    "strconv"
    "time"
)

func main() {
    file := flag.String("c", "config.txt", "Path to config file")
    flag.Parse()

    protocol, port, n, err := config.ReadOptionsFromFile(*file)

    if err != nil {
        fmt.Println(err)
        return
    }

    clientCount := 0
    allClients := make(map[net.Conn]int)
    newConnections := make(chan net.Conn)
    deadConnections := make(chan net.Conn)

    server, err := net.Listen(protocol, port)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    fmt.Println("Serverul așteaptă conexiuni...")
    go func() {
        for {
            conn, err := server.Accept()
            if err != nil {
                fmt.Println(err)
                os.Exit(1)
            }
            newConnections <- conn
        }
    }()

    for {

        select {

        case conn := <-newConnections:

            log.Printf("Client %d conectat", clientCount)

            allClients[conn] = clientCount
            clientCount += 1

            go func(conn net.Conn, clientId int) {
                reader := bufio.NewReader(conn)
                raw_numbers := make([]uint64, n)
                var i uint32
                for i = 0; i < n; i++ {
                    incoming, err := reader.ReadBytes('\n')
                    if err != nil {
                        break
                    }
                    raw_numbers[i] = uint64(binary.LittleEndian.Uint64(incoming))
                }

                fmt.Printf("Server a primit request cu datele `%d` de la clientul %d\n", raw_numbers, clientId)
                var sum uint64
                fmt.Println("Server procesează datele")
                for _, nr := range raw_numbers {
                    str_number := fmt.Sprintf("%d", nr)
                    intermediate_number := fmt.Sprintf("%c%s", str_number[0], str_number)
                    number, _ := strconv.ParseUint(intermediate_number, 10, 64)
                    sum += number
                }
                time.Sleep(5000 * time.Millisecond)
                fmt.Printf("Server trimite %d către client\n", sum)
                bytes := make([]byte, 65)
                binary.BigEndian.PutUint64(bytes, sum)
                bytes[64] = '\n'
                conn.Write(bytes)

                deadConnections <- conn

            }(conn, allClients[conn])


        case conn := <-deadConnections:
            log.Printf("Client %d deconectat", allClients[conn])
            delete(allClients, conn)
        }
    }
}
