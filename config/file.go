package config

import(
    // "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

func ReadOptionsFromFile(file string) (string, string, uint32, error) {
    data, err := ioutil.ReadFile(file)
    if err != nil {
        // fmt.Printf("Error: %s\n", err.Error())
        return "", "", 0, err
    }
    v := strings.Split(string(data), ",")
    protocol, port, nr := v[0], v[1], v[2]
    var n uint32
    num, err := strconv.ParseUint(nr, 10, 32)
    n = uint32(num)
    if err != nil {
        // fmt.Printf("Error parsing file %s: %s is not a number\n", file, nr)
        return "", "", 0, err
    }
    return protocol, port, n, err
}
